var express = require('express');
var router = express.Router();
var Store = require('../models/locations');


router.get('/perdate', function(req, res, next){
  res.render('earningsDate');
});

router.get('/earningsdate', function(req, res, next){
  stores = new Store();
  stores.getEarningsPerDate()
  .then ( data => {
    res.send(data);
  });
});

router.get('/all', function(req, res, next){
  stores = new Store();
  stores.listar()
  .then ( data => {
    res.send(data);
  });
});

router.get('/earningsemployee', function(req, res, next){
  stores = new Store();
  stores.getEarningsPerEmployee()
  .then ( data => {
    res.send(data);
  });
});

router.get('/itemsperstore', function(req, res, next){
  stores = new Store();
  stores.getItemSoldPerStore()
  .then ( data => {
    res.send(data);
  });
});

module.exports = router;
