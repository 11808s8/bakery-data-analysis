$(document).ready(function() {
    $.get('/store/itemsperstore', function(res) {
        categories_select(res) 
        .then(([categories_id, categories_names]) =>{
            var ids_separados = separa_ids(res);
            return [categories_names, ids_separados];
        })
        .then(([categories_names, ids_separados]) =>{
            var arrVendas = separa_vendas_por_id_goods(ids_separados,res);
            return [categories_names, arrVendas];
        })
        .then(([categories_names, array_columns]) =>{
            try{
                var config = {
                    bindto:'#chart1',
                    data: {
                        columns: 
                            array_columns
                        ,
                        type: 'bar'
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: categories_names,
                            extent: [5, 10]
                        }
                    },padding: {
                        bottom: 50
                      },
                    subchart: {
                        show: true
                    },
                    legend: {
                        position: 'right'
                    },
                    bar: {
                        width: {
                            ratio: 0.5 
                        }
                    }
                }
                var chart = c3.generate(config);
            }
            catch(err){
                console.log(err);
            }
        })
    });
  });

  function categories_select(res){
    return new Promise(function(resolve, reject){
        try{
        var category = [];
        var category_name = [];
        var notFound = true;
        for(var i=0; i < res.length;i++){
            for(var j=0;j<category.length;j++){
                if(category[j] === res[i].id_good){
                    notFound = false;
                    break;
                }
            }
            if(notFound==true)
            {
                category.push(res[i].id_good);
                category_name.push(res[i].name);  
            }
            else
                notFound = true;
        }
        resolve([category,category_name]);
        }
        catch(err){
            console.log(err);
            reject();
        }
    });  
  }


  function separa_ids(res){
    try{
        let arr = [];
        let found = false;
        for(let i=0; i<res.length;i++)
        {
            for(var j=0;j<arr.length;j++){
                if(res[i].id_store_location === arr[j][0]){
                    found=true;
                }
            }
            if(!found){
                
                arr.push([res[i].id_store_location])    
            }
            found=false;
        }
        return arr;
    }
    catch(err){
        console.log(err);
    }
  }


  function separa_vendas_por_id_goods(array_columns,res){
      try{
        for(let i=0;i<res.length;i++){
            for(var j=0;j<array_columns.length;j++){
                if(res[i].id_store_location === array_columns[j][0]){
                    array_columns[j].push(res[i].sold_price);
                    break;
                }
            }
        }
        return array_columns;
      }
      catch(err){
          console.log(err);
      }
  }