$(document).ready(function() {
    $.get('/store/earningsDate', function(res) {
        let array_columns = [];
        date_select(res)
        .then( data =>{
            array_columns = separa_ids(res);
            var arr = ['x'];
            arr = arr.concat(data);
            return [arr,array_columns];

        })
        .then(([arr,array_columns]) => {
            arrayFinal = separa_vendas_por_ids(array_columns, res);
            return [arr, arrayFinal];
        })
        .then(([arr, arrayFinal])=>{
            arrF = []
            arrF.push(arr);
            
            arrF = arrF.concat(arrayFinal);
            return [arrF];
        })
        .then(([array_columns]) => {
            try{
                var chart = c3.generate({
                    bindto:'#chart',
                    data: {
                        x : 'x',
                        columns: 
                            array_columns
                        ,
                        type: 'line'
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            tick: {
                                format: '%Y-%m-%d'
                            }
                        }
                    
                    },
                    subchart: {
                        show: true
                    },
                    legend: {
                        position: 'right'
                    }
                });
            }
            catch(err){
                console.log(err);
            }
        })
    });
  });

  function date_select(res){
    return new Promise(function(resolve, reject){
        try{
        var sale_date = [];
        var notFound = true;
        for(var i=0; i < res.length;i++)
                res[i].sale_date = res[i].sale_date.substring(0,10);
        
        for(var i=0; i < res.length;i++){
            for(var j=0;j<sale_date.length;j++){
                if(sale_date[j] === res[i].sale_date){
                    
                    notFound = false;
                    break;
                }
            }
            if(notFound==true)
            {
                sale_date.push(res[i].sale_date);  
            }
            else
                notFound = true;
        }
        resolve(sale_date);
        }
        catch(err){
            console.log(err);
        }
    });
      
  }


  function separa_ids(res){
    try{
        let arr = [];
        let found = false;
        for(let i=0; i<res.length;i++)
        {
            for(var j=0;j<arr.length;j++){
                if(res[i].id_store_location === arr[j][0]){
                    found=true;
                }
            }
            if(!found){
                
                arr.push([res[i].id_store_location])    
            }
            found=false;
        }
        return arr;
    }
    catch(err){
        console.log(err);
    }
  }


  function separa_vendas_por_ids(array_columns,res){
      try{
        for(let i=0;i<res.length;i++){
            for(var j=0;j<array_columns.length;j++){
                if(res[i].id_store_location === array_columns[j][0]){
                    array_columns[j].push(res[i].sold_price);
                    break;
                }
            }
        }
        return array_columns;
      }
      catch(err){
          console.log(err);
      }
  }