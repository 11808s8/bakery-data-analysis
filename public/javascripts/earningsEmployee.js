$(document).ready(function() {
    $.get('/store/earningsemployee', function(res) {
        
        let chartNumber=2;
        for(;chartNumber<res.length;chartNumber++)
            $('#multiple-charts').append("<div id=chart"+chartNumber +"></div>");
        employee_name_select(res) 
        .then(([employee_id, employee_names,employee_sold, employee_store, employee_store_name]) =>{
            employees_separados_por_store(employee_names, employee_sold, employee_store, employee_store_name)                      
            .then(employees_per_store =>{
                try{
                    for(var i=0;i<employees_per_store.length;i++){
                        new Promise(function(resolve, reject){
                            var bindToId='#chart'+(i+2);
                            var chart = c3.generate({
                                bindto:bindToId,
                                size: {
                                    height: 240,
                                    width: 480
                                },
                                data: {
                                    columns: 
                                        employees_per_store[i][2]
                                    ,
                                    type: 'bar'
                                },
                                title: {
                                    text: 'Store ' + employees_per_store[i][0] + ': ' + employees_per_store[i][1]
                                },
                                bar: {
                                    width: {
                                        ratio: 0.4
                                    }
                                }
                            });
                        })
                        
                    }
                }catch(err){
                    console.log(err);
                }
            })
        })
            
        
    });
  });

  function employee_name_select(res){
    return new Promise(function(resolve, reject){
        try{
        var employee = [], employee_sold = [], employee_store = [], 
        employee_store_name = [], employee_name = [];
        var notFound = true;
        for(var i=0; i < res.length;i++){
            for(var j=0;j<employee.length;j++){
                if(employee[j] === res[i].id_employee){
                    notFound = false;
                    break;
                }
            }
            if(notFound==true)
            {
                employee.push(res[i].id_employee);
                employee_name.push(res[i].full_name);
                employee_sold.push(res[i].sold_price);
                employee_store.push(res[i].id_store_location);  
                employee_store_name.push(res[i].location_name);
            }
            else
                notFound = true;
        }
        resolve([employee,employee_name, employee_sold, employee_store,employee_store_name]);
        }
        catch(err){
            console.log(err);
            reject();
        }
    });  
  }

  function employees_separados_por_store(employee_names, employee_sold, employee_store,employee_store_name){
    let arrayzao = [];
    let storesPresentesArr = [];
    let auxArr = [];
    let foundStore=false;
    return new Promise(function(resolve, reject){
        try{
            for(var i=0; i<employee_store.length;i++){
                for(var j=0;j<storesPresentesArr.length;j++){
                    if(employee_store[i]==storesPresentesArr[j]){
                        foundStore = true;
                        break;
                    }
                }
                if(!foundStore){
                    auxArr.push(employee_store[i]);
                    auxArr.push(employee_store_name[i]);
                    let auxPerStoreEmployeesArr = [];
                    let auxDataArr = [];
                    for(var k=0;k<employee_names.length;k++){
                        if(employee_store[i]==employee_store[k]){
                            auxPerStoreEmployeesArr.push(employee_names[k]);
                            auxPerStoreEmployeesArr.push(employee_sold[k]);
                            auxDataArr.push(auxPerStoreEmployeesArr);
                            auxPerStoreEmployeesArr = [];
                        }
                    }
                    auxArr.push(auxDataArr);
                    arrayzao.push(auxArr);
                    storesPresentesArr.push(employee_store[i]);
                }
                foundStore=false;
                auxArr = [];
            }
            resolve(arrayzao);
        }catch(err){
            console.log(err);
            reject();
        }
    });
  }
