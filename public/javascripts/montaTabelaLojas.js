/*
    Função que trata os dados do select * das lojas e monta uma tabela boostrap
*/
$(document).ready(function() {
    $.get('/store/all', function(res) {
        for(var i=0; i< res.length;i++){
            $('.tabela-lojas-corpo').append("<tr><th scope='row'>" + res[i].id_store_location +"</th><td>" + res[i].street +"</td><td>" + res[i].city +"</td><td>" + res[i].state +"</td><td>" + res[i].zip_code +"</td></tr><tr>")
        }
    });
});