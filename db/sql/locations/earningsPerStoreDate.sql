select SEL.id_store_location, loc.zip_code, SEL.sale_date, SEL.sold_price
from(
    select rec.id_store_location, rec.sale_date, sum(sal.quantity * goo.price) as sold_price
    from ${schema~}.receipts rec
    inner join ${schema~}.sales sal on sal.id_receipt = rec.id_receipt
    inner join ${schema~}.goods goo on goo.id_good = sal.id_good
    group by (rec.id_store_location, rec.sale_date)
) as SEL
inner join ${schema~}.locations loc on loc.id_store_location = SEL.id_store_location
order by SEL.sale_date;