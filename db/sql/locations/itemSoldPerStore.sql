select SEL.id_store_location, SEL.id_good, SEL.sold_price, fla.name || ' ' || foo.name as name
from(
    select rec.id_store_location, sal.id_good, goo.id_food_type, goo.id_flavor, sum(sal.quantity * goo.price) as sold_price
    from ${schema~}.receipts rec
    inner join ${schema~}.sales sal on sal.id_receipt = rec.id_receipt
    inner join ${schema~}.goods goo on goo.id_good = sal.id_good
    group by (rec.id_store_location, sal.id_good, goo.id_food_type, goo.id_flavor)
) as SEL
inner join ${schema~}.flavors fla on fla.id_flavor = SEL.id_flavor
inner join ${schema~}.food_types foo on foo.id_food_type = SEL.id_food_type
order by SEL.id_store_location, SEL.id_good
--order by SEL.id_good