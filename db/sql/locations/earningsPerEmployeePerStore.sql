select SEL.id_store_location, loc.street || ' - ' || loc.city || ' - ' || loc.state as location_name , SEL.id_employee, emp.first_name || ' ' || emp.last_name as full_name, SEL.sold_price
from(
    select rec.id_store_location, rec.id_employee, sum(sal.quantity * goo.price) as sold_price
    from ${schema~}.receipts rec
    inner join ${schema~}.sales sal on sal.id_receipt = rec.id_receipt
    inner join ${schema~}.goods goo on goo.id_good = sal.id_good
    group by (rec.id_store_location, rec.id_employee)
) as SEL
inner join ${schema~}.employees emp on emp.id_employee = SEL.id_employee
inner join ${schema~}.locations loc on SEL.id_store_location = loc.id_store_location
order by SEL.id_store_location, SEL.id_employee