
const sql = require('../helper');

module.exports = {
    locations:{
        add: sql('locations/add.sql'),
        any: sql('locations/any.sql'),
        find: sql('locations/find.sql'),
        earningsDate: sql('locations/earningsPerStoreDate.sql'),
        itemSoldPerStore: sql('locations/itemSoldPerStore.sql'),
        earningsEmployee: sql('locations/earningsPerEmployeePerStore.sql')
    }
};
