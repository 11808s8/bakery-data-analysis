const sql =  require("../sql").locations;

module.exports = class locationsDao {

    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;
    }

    getStore(id){
        return this.db.oneOrNone(sql.find, [
            id]
        );
        
    }

    getEarningsDate(){
        return this.db.manyOrNone(sql.earningsDate);
    }

    getEarningsEmployee(){
        return this.db.manyOrNone(sql.earningsEmployee);
    }

    getItemSoldPerStore(){
        return this.db.manyOrNone(sql.itemSoldPerStore);
    }

    setStore(city, state, zip_code, street, lat, lng){
        this.db.none(sql.add, [city, state, zip_code, street, lat, lng]);
    }

    listar() {
        return this.db.manyOrNone(sql.any);
    }
    // outros métodos de persistência
}