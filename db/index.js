

// Repositório com as consultas
const dataObjects = require('./dataObjects');

// Opções de inicialização do pgp
const initOptions = {
    
    extend(obj, dc) {
        obj.store = new dataObjects.Locations(obj, pgp);
;    }
};

// Configurações de acesso ao banco
const config = {
    host: 'localhost',
    port: 5432,
    database: 'trab_helena',
    user: 'postgres'
};

// Inicialização
const pgp = require('pg-promise')(initOptions);
const db = pgp(config);
const diagnostics = require('./diagnostics');
diagnostics.init(initOptions);

module.exports = db;
