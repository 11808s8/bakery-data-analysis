
///////////////////////////////////////////////
// Helper for linking to external query files;


const path = require('path');
const QueryFile = require('pg-promise').QueryFile;

module.exports = function sql(file) {

    const fullPath = path.join(__dirname + '/sql/', file); // generating full path;

    const options = {

        // minifying the SQL is always advised;
        // see also option 'compress' in the API;
        minify: true,

        // Showing how to use static pre-formatting parameters -
        // we have variable 'schema' in each SQL (as an example);
        params: {
            schema: 'public' // replace ${schema~} with "public"
        },

        error: (err, e) => {
            if (err instanceof QueryResultError) {
                
                console.log(err);
                
                // See also: err, e.query, e.params, etc.
            }
        }

    };

    const qf = new QueryFile(fullPath, options);

    if (qf.error) {
        // Something is wrong with our query file :(
        // Testing all files through queries can be cumbersome,
        // so we also report it here, while loading the module:
        console.error(qf.error);
    }

    return qf;

    // See QueryFile API:
    // http://vitaly-t.github.io/pg-promise/QueryFile.html
}
