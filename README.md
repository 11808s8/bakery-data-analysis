# Projeto de Análise de Dados utilizando o Banco de Dados de uma Padaria fictícia

### Must Have
* node.js e npm instalados

## Rodando o projeto: <br>
*   Caso ainda não possua o projeto em sua máquina execute o seguinte comando: <br> `git clone https://gitlab.com/11808s8/bakery-data-analysis.git` <br>
*   Com o repositório clonado, acesse-o e execute o comando `npm install` <br>
*   Feito isto, os módulos de dependências do projeto estarão instalados e para rodar o projeto, <br>no diretório principal do mesmo (o qual contém o arquivo app.js), execute o comando `npm start`<br>
*   Acesse o projeto no seguinte endereço, pelo seu navegador: localhost:3000