var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// require('dotenv').config();
var indexRouter = require('./routes/index');
var storeRouter = require('./routes/store');


module.exports = db = require('./db');


var app = express();

app.locals.siteName = "Bakery Data Analysis";
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist/'));
app.use('/d3', express.static(__dirname + '/node_modules/d3/dist/'));
app.use('/c3', express.static(__dirname + '/node_modules/c3/'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/store', storeRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
