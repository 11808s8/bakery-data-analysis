
module.exports = class Locations{
    
    constructor(){
    }

    getStore(id){
        return db.store.getStore(id);
    }

    getEarningsPerDate(){
        return db.store.getEarningsDate();
    }

    getEarningsPerEmployee(){
        return db.store.getEarningsEmployee();
    }

    getItemSoldPerStore(){
        return db.store.getItemSoldPerStore();
    }

    listar(){
        var lista = db.store.listar();
        return lista;
    }
}